import logo from './logo.svg';
import './App.css';
import { useCallback, useEffect, useRef, useState } from 'react';

const question = require('./cdl.json')
function App() {
  const [index, setIndex] = useState(0)
  const [totalSeconds, setTotalSeconds] = useState(0)
  const [isShowAnswer, setIsShowAnswer] = useState(false)
  const [input, setInput] = useState(0)
  const timeout = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  const [intervalId, setIntervalId] = useState();

  const setTime = (totalSecondsTemp) => {
    setTotalSeconds(totalSecondsTemp)
  }


  const pad = (val) => {
    var valString = val + "";
    if (valString.length < 2) {
      return "0" + valString;
    } else {
      return valString;
    }
  }

  useEffect(() => {
    const imagesPreload = question.filter((x) => x.image).map((x) => `/img/${x.image}`);
    imagesPreload.forEach((image) => {
      const newImage = new Image();
      newImage.src = image;
      window[image] = newImage;
    });

    // let totalSecondsTemp = 0
    // const id = setInterval(() => {
    //   setTime(++totalSecondsTemp)
    // }
    // , 1000);
    // setIntervalId(id);
    // async function play() {
    //   for (let i = 0; i < question.length; i++) {
    //     await nextQuest(i)
    //   }
    // }
    // play()
  }, [])

  const getCurrentQuestion = (newPlaySeconds, indexTemp) => {
    let currentIndex = indexTemp
    let isAnswer = false
    let duration = 0;
    for (let i = indexTemp; i < question.length; i++) {
      duration += question[i].duration_question + question[i].duration_exp;
      if (newPlaySeconds <= duration) {
        currentIndex = i
        if ((question[i].duration_exp > 0 && duration - newPlaySeconds <= (question[i].duration_exp + 3)) || (question[i].duration_exp === 0 && duration - newPlaySeconds <= 6)) isAnswer = true
        break
      }
    }
    return {
      currentIndex,
      isAnswer
    }
  }



  const Play = () => {
    let totalSecondsTemp = totalSeconds
    const indexTemp = index
    let newPlaySeconds = 0
    const id = setInterval(() => {
      setTime(totalSecondsTemp)
      const currentQuestion = getCurrentQuestion(newPlaySeconds, indexTemp)
      setIndex(currentQuestion.currentIndex)
      setIsShowAnswer(currentQuestion.isAnswer)
      newPlaySeconds++
      totalSecondsTemp++
    }
      , 1000);
    setIntervalId(id);
  }

  const Stop = () => {
    clearInterval(intervalId)
  }

  return (
    <div style={{ padding: 36, fontSize: 34 }} >
      <div style={{ display: 'flex', marginBottom: 5, alignItems: 'center' }}>
        <div>{`Timer: ${pad(parseInt(totalSeconds / 60))}:${pad(totalSeconds % 60)}`}</div>
        <div>
          <div style={{ display: 'flex', marginLeft: 10 }}>
            <button onClick={Play} className="btn">Play</button> <button className="btn" onClick={Stop}>Stop</button>
            <input style={{ marginLeft: 50 }} type='number' min={1} max={question.length} onChange={(e) => setInput(e.target.value)} /> <button className="btn" onClick={() => setIndex(parseFloat(input) - 1)}>Go to question</button>

          </div>
        </div>

      </div>
      <div className='container-app' style={{ position: 'relative', justifyContent: 'center', display: 'flex' }}>
        <div style={{ width: 1200 }}>
        {/* <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <h5 style={{ color: '#212121', margin: '20px 0px 0px' }}>CDL Prep</h5>
        </div>

        <div style={{ 
          width: '100%', 
          height: 1, 
          borderBottom: '2px solid rgba(244, 244, 244, 1)',
          marginTop: 40,
        }} /> */}
        
        <h4 style={{ color: '#212121', marginBottom: 68 }}>{`${index + 1}.`} &nbsp; {`${question[index].question}`}</h4>
        <div style={{ display: 'flex' }}>
          {
            question[index].image &&
            <img style={{ height: 280 }} src={`/img/${question[index].image}`} alt="img" />
          }
          <div style={{ paddingLeft: 72, paddingRight: 72 }}>
            <label
              style={{ color: isShowAnswer && question[index].answer === 1 ? '#6b972e' : isShowAnswer && question[index].answer !== 1 ? '#6f7d8d' : '#6f7d8d' }} class="container">A.&nbsp;&nbsp;&nbsp;{`${question[index].o1}`}
              <input type="radio" name="radio" checked={isShowAnswer && question[index].answer === 1} />
              <span class="checkmark"></span>
            </label>
            <label style={{ color: isShowAnswer && question[index].answer === 2 ? '#6b972e' : isShowAnswer && question[index].answer !== 2 ? '#6f7d8d' : '#6f7d8d' }} class="container">B.&nbsp;&nbsp;&nbsp;{`${question[index].o2}`}
              <input type="radio" name="radio" checked={isShowAnswer && question[index].answer === 2} />
              <span class="checkmark"></span>
            </label>
            <label style={{ color: isShowAnswer && question[index].answer === 3 ? '#6b972e' : isShowAnswer && question[index].answer !== 3 ? '#6f7d8d' : '#6f7d8d' }} class="container">C.&nbsp;&nbsp;&nbsp;{`${question[index].o3}`}
              <input type="radio" name="radio" checked={isShowAnswer && question[index].answer === 3} />
              <span class="checkmark"></span>
            </label>
            <label style={{ color: isShowAnswer && question[index].answer === 4 ? '#6b972e' : isShowAnswer && question[index].answer !== 4 ? '#6f7d8d' : '#6f7d8d' }} class="container">D.&nbsp;&nbsp;&nbsp;{`${question[index].o4}`}
              <input type="radio" name="radio" checked={isShowAnswer && question[index].answer === 4} />
              <span class="checkmark"></span>
            </label>
          </div>
        </div>
        {
          isShowAnswer &&
          <div className='explanation'>
            {/* <div>Explanation:</div> */}
            <div>            {`${question[index].explanation}`}
</div>
          </div>
        }
        </div>
        

          <div style={{ 
            display: 'flex', 
            position: 'absolute',
            bottom: 36,
            right: 36,
          }}>
            <img style={{ height: 50 }} src={`/logo.png`} alt="logo" />
            
          </div>
      </div>
    </div>
  );
}

export default App;
